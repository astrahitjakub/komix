export const arraySum = (inputArray) =>
  inputArray.reduce(
    (previousValue, currentValue) =>
      previousValue +
      (Array.isArray(currentValue) ? arraySum(currentValue) : currentValue),
    0
  );
