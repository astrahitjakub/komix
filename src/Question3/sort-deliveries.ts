import { Delivery } from "./network";

const STATUS_RANK = ["active", "upcoming", "pending"];

export const sortDeliveries = (item1: Delivery, item2: Delivery) => {
  const compareByStatus =
    STATUS_RANK.indexOf(item1.status) - STATUS_RANK.indexOf(item2.status);
  return compareByStatus !== 0
    ? compareByStatus
    : (item1.eta ?? 0) - (item2.eta ?? 0);
};
