const addModule = (() => {
  let sum = 0;
  const context = {
    addToSum: (value) => {
      sum = sum + value;
      return sum;
    },
  };
  return context;
})();

export const add = (value) => addModule.addToSum(value);
