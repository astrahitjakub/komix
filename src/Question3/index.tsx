import React from "react";
import { Delivery, getDeliveries } from "./network";
import { sortDeliveries } from "./sort-deliveries";

export const Question3: React.FC = () => {
  const [error, setError] = React.useState(false);
  const [loading, setLoading] = React.useState(false);
  const [data, setData] = React.useState<Delivery[]>([]);

  React.useEffect(() => {
    let mounted = true;
    setLoading(true);
    getDeliveries()
      .then((items) => {
        if (mounted) {
          setData(items.sort(sortDeliveries));
          setLoading(false);
        }
      })
      .catch(() => {
        if (mounted) {
          setError(true);
          setLoading(false);
        }
      });
    return () => {
      mounted = false;
    };
  }, []);

  if (loading) {
    return <div>Loading...</div>;
  }

  if (error) {
    return <div>Error occurred.</div>;
  }

  return (
    <table className="table table-striped table-hover">
      <thead className="thead-dark">
        <tr>
          <th scope="col">ID</th>
          <th scope="col">Name</th>
          <th scope="col">Amount</th>
          <th scope="col">ETA</th>
          <th scope="col">Status</th>
        </tr>
      </thead>

      <tbody>
        {data.map((item) => (
          <tr key={`delivery-${item.id}`}>
            <td>{item.id}</td>
            <td>{item.name}</td>
            <td>{item.amount}</td>
            <td>{item.eta}</td>
            <td>{item.status}</td>
          </tr>
        ))}
      </tbody>
    </table>
  );
};
