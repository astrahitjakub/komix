import { arraySum } from ".";

describe("question 1", () => {
  test("get question right", () => {
    const arr = [[3, 2], [1], [4, 12], [2, [3, 7]]];
    const expected = 34;
    const result = arraySum(arr);
    expect(result).toEqual(expected);
  });

  test("get question right with more nested arrays", () => {
    const arr = [
      [3, [2, 5, [4, 8]]],
      [1, [2, [3, 7]]],
      [4, 12],
      [2, [3, 7]]
    ];
    const expected = 63;
    const result = arraySum(arr);
    expect(result).toEqual(expected);
  });
});
